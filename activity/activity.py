test_yr_num  = int(input("Please Enter a YEAR!!: "))

test_row_num = int(input("Please Enter the row number: "))
test_col_num = int(input("Please Enter the col number: "))

# For Asteriks Activity

for x in range(test_col_num):
	print('*' * test_row_num + '')


# For Leap Year Activity
if test_yr_num <= 0:
	print(f"ERROR: The year {test_yr_num} is ZERO or NEGATIVE value")
else:
	if test_yr_num % 4 != 0:
		print(f"The year {test_yr_num} is NOT a Leap Year!")
	elif test_yr_num % 100 == 0 and test_yr_num % 400 != 0:
		print(f"The year {test_yr_num} is NOT a Leap Year!")
	else:
		print(f"The year {test_yr_num} is a Leap Year!")