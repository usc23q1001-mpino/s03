# Input
# Input() allows us to gather data from the user input, returns "string" data type
# "\n" stands for line break

#username = input("PLEASE ENTER YOUR NAME: \n")
#printf(f"Hi {username}! Welcome to Python Short Course")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# If-else statements
# If-else statements are used to choose between two or more code blocks depending on the condition

# Declare a variable to use for the conditional statement

#test_num = 75

#if test_num >= 60:
#	print("Test Passed.")
#else:
#	print("Test Failed.")

# Else-if chains
# test_num2 = int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
# 	print("The number is POSITIVE")
# elif test_num2 == 0:
# 	print("The number is ZERO")
# else:
# 	print("The number is NEGATIVE")

# Mini-Exercise:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both.

# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
# If the number is  NOT divisible by any, print "The number is  NOT divisible by 3 NOR 5"


#num1 = int(input("Enter 1st number: \n"))

#if num1 % 3 == 0 & num1 % 5 == 0:
#	print("The number is divisible by BOTH 3 and 5")
#elif num1 % 5 == 0:
#	print("The number is divisible by 5")
#elif num1 % 3 == 0:
#	print("The number is divisible by 3")
#else:
#	print("The number is NOT divisible by 3 NOR 5")
#	pass


# Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statement as long as the condition is true

#i = 25
#while i <= 5:
#	print("Current Count{i}")
#	i += 1
# For Loops are used for iterating over a sequence
#fruits = ["apple", "Orange", "Bayabas"]

#for indiv_fruit in fruits:
#	print(indiv_fruit)

for x in range(6):
	print(f"The Current value is {x}")

# Syntax:
# range(stop)
# range(start, stop)
# range(start, stop, step)

# Break Statements
# Continue Statements

